<?php

//using iterable as a function argument
// function printIterable(iterable $iterables)
// {
//     foreach ($iterables as $iterable) {
//         echo $iterable;
//     }
// }

// $names = ['Simanta', 'Poudel', 'Lokanthali', 'Bhaktapur'];
// $movies = ['Avatar', 'Conjuring', 'Joker'];
// printIterable($names);
// echo "<br>";
// printIterable($movies);

// $brands = ['Apple', 'Samsung', 'LG', 'Dell'];
// $movies = ['Avatar', 'Conjuring', 'Joker'];
// $books = ['Alchemist', 'Karnali Blues', 'Operating Systems'];

// // foreach ($brands as $name) {
// //     echo $name;
// // }

// // echo "<br>";

// // foreach($movies as $movie) {
// //     echo $movie;
// // }

// // echo "<br>";

// // foreach ($books as $book) {
// //     echo $book;
// // }

// function printIterables(iterable $iterables)
// {
//     foreach ($iterables as $iterable) {
//         echo $iterable . "<br>";
//     }
// }

// printIterables($brands);
// printIterables($movies);
// printIterables($books);

// using iterable as return type of a function
function getIterable(): iterable
{
    return ['Apple', 'Samsung', 'LG', 'Dell'];
}

$iterable = getIterable();
var_dump($iterable);