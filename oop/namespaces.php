<?php

/** Namespace => namespace is a collection of classes that work together to perform a task
 * 
 */

namespace AutoMobile;

class Vehicle 
{
    public $type = "Car";
    public $brand = "Ferrari";
    public $name = "Portofino";
    public function displayVehicleDesc()
    {
        echo "{Description of Vehicle: $this->type $this->brand $this->name}"; 
    }
}

class Manufacturer
{
    public $name = "Ferrari S.P.A.";
    public $type = "Luxury Cars";
    public $estdate = "1939";
    public function displayManufacturerDesc()
    {
        echo "{Description of Vehicle: $this->name $this->estdate $this->type}"; 
    }
}